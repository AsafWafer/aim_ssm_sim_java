package com.wafer.aim;

public class Coordinate {

	public Coordinate() {
		azimuthDegree = "121.3";
		elevationDegree = "124.7";
		gpsArrivalTimeWeek = 1433;
		gpsArrivalTimeUs = 396049700000L;
		dwellTimeMs = 820;
	}
	
	public String azimuthDegree;
	public String elevationDegree;
	public Integer gpsArrivalTimeWeek;
	public Long gpsArrivalTimeUs;
	public Integer dwellTimeMs;

}
