package com.wafer.aim;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Random;
import java.awt.event.ActionEvent;
import com.google.gson.*;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import java.awt.TextArea;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.poi.*;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SSM_SIM_Tack implements TableModelListener {

	///////////////////////////////////////////////////////
	// privates
	///////////////////////////////////////////////////////
	private JFrame frmSsmSimulatorTrack;
	private TextField azimuth_degree = new TextField();
	private Track track = new Track();	
	private Button buttonSend = new Button("Send");
	private TextArea textAreaJSON = new TextArea();
	private TextField elevation_degree = new TextField();	
	private TextField textFieldGpsArrivalTimeWeek = new TextField();		
	private TextField textFieldGpsArrivalTimeUs = new TextField();
	private TextField textFieldDwellTimeMs = new TextField();
	private JTable table;
	private JFileChooser chooser;

	///////////////////////////////////////////////////////
	// constants
	///////////////////////////////////////////////////////
	private final float D_X = (float) 10.3;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SSM_SIM_Tack window = new SSM_SIM_Tack();
					window.frmSsmSimulatorTrack.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SSM_SIM_Tack() {
		initialize();
	}

	private String getJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String json = gson.toJson(track);

		//	      JsonParser parser = new JsonParser();
		//	      JsonObject jsonObject = parser.parse(json).getAsJsonObject();
		//
		//	      Gson gson = new GsonBuilder().setPrettyPrinting().create();
		//	      String prettyJson = gson.toJson(json);		
		return json;
	}

	private void parseJSON(String json) {
		Gson gson = new Gson();

		track = gson.fromJson(json, Track.class);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSsmSimulatorTrack = new JFrame();
		frmSsmSimulatorTrack.setTitle("SSM simulator - Track");
		frmSsmSimulatorTrack.setBounds(100, 100, 720, 797);
		frmSsmSimulatorTrack.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSsmSimulatorTrack.getContentPane().setLayout(null);


		buttonSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String       postUrl       = "www.site.com";// put in your url
				Gson         gson          = new Gson();
				HttpClient   httpClient    = HttpClientBuilder.create().build();
				HttpPost     post          = new HttpPost(postUrl);
				StringEntity postingString;
				try {
					postingString = new StringEntity(gson.toJson(track));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}//gson.tojson() converts your pojo to json
				post.setEntity(postingString);
				post.setHeader("Content-type", "application/json");
				try {
					HttpResponse  response = httpClient.execute(post);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		buttonSend.setBounds(10, 382, 79, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonSend);

		Label label = new Label("azimuth_degree");
		label.setBounds(10, 10, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(label);


		azimuth_degree.setText("112.1");
		azimuth_degree.setBounds(170, 10, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(azimuth_degree);

		Label label_1 = new Label("elevation_degree");
		label_1.setBounds(10, 40, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(label_1);


		elevation_degree.setText("112.1");
		elevation_degree.setBounds(170, 40, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(elevation_degree);
		textAreaJSON.setFont(new Font("Dialog", Font.PLAIN, 15));
		textAreaJSON.setText(getJSON());

		textAreaJSON.setBounds(10, 203, 682, 190);
		frmSsmSimulatorTrack.getContentPane().add(textAreaJSON);

		Button buttonReset = new Button("Reset");
		buttonReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				track.body.coordinates.clear();
				textAreaJSON.setText(getJSON());
			}
		});
		buttonReset.setBounds(613, 173, 79, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonReset);

		Button buttonAddCoordinate = new Button("Add coordinate");
		buttonAddCoordinate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Coordinate coordinate = new Coordinate();
				coordinate.azimuthDegree = azimuth_degree.getText();
				coordinate.elevationDegree = elevation_degree.getText();
				coordinate.dwellTimeMs = Integer.parseInt(textFieldDwellTimeMs.getText());
				coordinate.gpsArrivalTimeUs = Long.parseLong(textFieldGpsArrivalTimeUs.getText());

				track.body.coordinates.add(coordinate);
				textAreaJSON.setText(getJSON());
			}
		});
		buttonAddCoordinate.setBounds(10, 173, 112, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonAddCoordinate);

		Label label_2 = new Label("gpsArrivalTimeWeek");
		label_2.setBounds(10, 70, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(label_2);


		textFieldGpsArrivalTimeWeek.setText("1433");
		textFieldGpsArrivalTimeWeek.setBounds(170, 70, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(textFieldGpsArrivalTimeWeek);

		Label label_3 = new Label("gpsArrivalTimeUs");
		label_3.setBounds(10, 100, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(label_3);

		textFieldGpsArrivalTimeUs.setText("396049700000");
		textFieldGpsArrivalTimeUs.setBounds(170, 100, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(textFieldGpsArrivalTimeUs);

		Label label_4 = new Label("dwellTimeMs");
		label_4.setBounds(10, 130, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(label_4);


		textFieldDwellTimeMs.setText("820");
		textFieldDwellTimeMs.setBounds(170, 130, 143, 24);
		frmSsmSimulatorTrack.getContentPane().add(textFieldDwellTimeMs);

		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Float(112.1), new Float(0), new Float(360), new Float(0.1)));
		spinner.setBounds(331, 12, 143, 22);
		frmSsmSimulatorTrack.getContentPane().add(spinner);

		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		Object[][] data = new Object[16][16];
		Random r = new Random();
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				float random = r.nextFloat() * (10);
				data[i][j] = Math.round(random * 10.0) / 10.0;
			}
		}
		table.setModel(new DefaultTableModel(
				data,
				new String[] {
						"1", "2", "3", "4", "5", "6", "7", "8", "9", "19", "11", "12", "13", "14", "15", "16"
				}
				));

		table.setBounds(145, 399, 462, 368);
		table.setRowHeight(20);
		table.getModel().addTableModelListener(this);
		frmSsmSimulatorTrack.getContentPane().add(table);

		// calibration table holde the voltage value for every phy
		float[] calibrationTable = new float[360];
		for (int i = 0; i < calibrationTable.length; i++) {
			calibrationTable[i] = (float) (10.5 / calibrationTable.length * i);
		}

		Button buttonCalculate = new Button("Calculate");
		buttonCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object[][] data = new Object[16][16];
				for (int i = 0; i < data.length; i++) {
					for (int j = 0; j < data[i].length; j++) {
						float val = calcPSval(i,j,azimuth_degree.getText());
						data[i][j] = Math.round(val * 10.0) / 10.0;
					}
				}
				table.setModel(new DefaultTableModel(
						data,
						new String[] {
								"1", "2", "3", "4", "5", "6", "7", "8", "9", "19", "11", "12", "13", "14", "15", "16"
						}
						));				
			}

			private float calcPSval(int i, int j, String azimuth) {
				float channel = (float) 12.7;
				float phy = calcPhy(i,j, (float) Math.toRadians(Float.parseFloat(azimuth)), channel);

				return getPScalibration(phy);
			} 


			private float getPScalibration(float phy) {
				int phyIndex = (int) (phy * 360 / (2 * Math.PI)); 
				phyIndex %= 360;
				return calibrationTable[phyIndex];
			}

			private float calcPhy(int i, int j, float azimuthRadians, float channel) {
				return (float) (i * 2 * Math.PI / (300 / channel) * D_X * Math.sin(azimuthRadians));
			}
		});
		buttonCalculate.setBounds(10, 412, 79, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonCalculate);

		Button buttonExport = new Button("Export");
		buttonExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chooser = new JFileChooser(); 
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Export To");
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				//
				// disable the "All files" option.
				//
				chooser.setAcceptAllFileFilterUsed(false);
				FileFilter ff = new FileNameExtensionFilter("XLSX Files", new String[] { "xlsx" });
				chooser.addChoosableFileFilter(ff);
				chooser.setFileFilter(ff);

				//    
				if (chooser.showSaveDialog(buttonExport) == JFileChooser.APPROVE_OPTION) { 
					System.out.println("getCurrentDirectory(): " 
							+  chooser.getCurrentDirectory());
					System.out.println("getSelectedFile() : " 
							+  chooser.getSelectedFile());

					String path = chooser.getSelectedFile().toString();
					try {
						File file = chooser.getSelectedFile();

						int i = file.getName().lastIndexOf('.');
						if (i == 0) {
							path = path + ".xlsx";
						}
						
						writeToExcell(table, path);
					} catch (IOException e) {

						e.printStackTrace();
					}
				}
				else {
					System.out.println("No Selection ");
				}

			}
		});
		buttonExport.setBounds(10, 442, 79, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonExport);
		
		Button buttonImport = new Button("Import");
		buttonImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chooser = new JFileChooser(); 
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Import From");
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				//
				// disable the "All files" option.
				//
				chooser.setAcceptAllFileFilterUsed(false);
				FileFilter ff = new FileNameExtensionFilter("XLSX Files", new String[] { "xlsx" });
				chooser.addChoosableFileFilter(ff);
				chooser.setFileFilter(ff);

				//    
				if (chooser.showOpenDialog(buttonExport) == JFileChooser.APPROVE_OPTION) { 
					System.out.println("getCurrentDirectory(): " 
							+  chooser.getCurrentDirectory());
					System.out.println("getSelectedFile() : " 
							+  chooser.getSelectedFile());

					String path = chooser.getSelectedFile().toString();
					try {

						
						ReadFromExcell(table, path);
					} catch (IOException e) {

						e.printStackTrace();
					}
				}
				else {
					System.out.println("No Selection ");
				}

			}
		});
		buttonImport.setBounds(10, 472, 79, 24);
		frmSsmSimulatorTrack.getContentPane().add(buttonImport);
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		int row = e.getFirstRow();
		int column = e.getColumn();
		TableModel model = (TableModel)e.getSource();
		String columnName = model.getColumnName(column);
		Object data = model.getValueAt(row, column);

		// Do something with the data...
		float f = Float.parseFloat(data.toString());
		textAreaJSON.setText(Float.toString(f));

	}

	private static void ReadFromExcell(JTable table, String path) throws FileNotFoundException, IOException {	
		File myFile = new File(path.toString());
		FileInputStream fis = new FileInputStream(myFile); 
		// Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); 
		// Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheetAt(0); 
		// Get iterator to all the rows in current sheet 
		Iterator<Row> rowIterator = mySheet.iterator(); 

		TableModel model = table.getModel(); //Table model 
		
		// Traversing over each row of XLSX file 
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through each columns
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				switch (cell.getCellTypeEnum()) {
				case STRING:
					System.out.print(cell.getStringCellValue() + "\t");
					break;

				case NUMERIC:
					System.out.print(cell.getNumericCellValue() + "\t");
					break;
					
				case BOOLEAN:
					System.out.print(cell.getBooleanCellValue() + "\t");
					break;
				default:
				}
			}
			System.out.println("");
		}
	}

	
	private static void writeToExcell(JTable table, String path) throws FileNotFoundException, IOException {
		try (XSSFWorkbook wb = new XSSFWorkbook()) {
			XSSFSheet sheet = wb.createSheet(); //WorkSheet
			XSSFRow row; // = sheet.createRow(2); //Row created at line 3
			TableModel model = table.getModel(); //Table model 

//			XSSFRow headerRow = sheet.createRow(0); //Create row at line 0
//			for(int headings = 0; headings < model.getColumnCount(); headings++){ //For each column
//				headerRow.createCell(headings).setCellValue(model.getColumnName(headings));//Write column name
//			}

			for(int rows = 0; rows < model.getRowCount(); rows++){ //For each table row
				//Set the row to the next one in the sequence 
				row = sheet.createRow((rows + 1)); 
								
				for(int cols = 0; cols < table.getColumnCount(); cols++){ //For each table column
					row.createCell(cols).setCellValue(model.getValueAt(rows, cols).toString()); //Write value
				}

			}
			wb.write(new FileOutputStream(path.toString()));//Save the file     
		}
	}
}
