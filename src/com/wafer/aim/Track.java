/**
 * @author Asaf Meir Bariv
 * @date 25/02/2018
 */

package com.wafer.aim;


import java.util.LinkedList;
import java.util.List;



public class Track {
	
	public Track() {
	
		header = new Header();
		body = new Body();	
	}
	
	public class Body {

	public Body() {
		ifPathId = 123;
		coordinates = new LinkedList<Coordinate>();
		coordinates.add(new Coordinate());
	}
	public Integer ifPathId;
	public List<Coordinate> coordinates = null;

	}
	



	public class Header {

	public String type = "request";
	public String message = "track";
	public Integer tid = 4;

	}
	

	public Header header;
	public Body body;
	
}

